package problems.p3.solution

import problems.p3.Node

/**
 * Given the root to a binary tree, implement problems.p3.solution.serialize(root), which serializes the tree into a string, and problems.p3.solution.deserialize(s),
 * which deserializes the string back into the tree.
 *
 * For example, given the following problem.Node class
 *
 * class problem.Node:
 *   def __init__(self, val, left=None, right=None):
 *     self.val = val
 *     self.left = left
 *     self.right = right
 *
 * The following test should pass:
 *
 * node = problem.Node("root", problem.Node("left", problem.Node("left.left")), problem.Node("right"))
 * assert problems.p3.solution.deserialize(problems.p3.solution.serialize(node)).left.left.val == "left.left"
 */
fun main() {
    val node =
            Node("root", Node("left", Node("left.left")), Node("right"))
    assert(deserialize(serialize(node))?.left?.left?.value == "left.left")
}

private fun serialize(node: Node?): String {
    println("S Input: $node")

    val output =
            if (node == null) "null"
            else "{value=\"${node.value}\",left=${serialize(node.left)},right=${serialize(
                    node.right
            )}}"

    println("S Output: $output")
    return output
}

private fun deserialize(node: String): Node? {
    println("D Input: $node")

    if (node.isBlank() || node == "null") return null
    val dsValue: String?
    val dsRight: Node?
    val dsLeft: Node?
    var str = node.substring(1).substring(0, node.length - 2).removePrefix("value=\"")
    dsValue = str.substring(0, str.indexOf("\",left="))
    str = str.removePrefix("$dsValue\",left=")
    var leftEndIndex = -1
    if (str.startsWith('{')) {
        var oCount = 0
        var cCount = 0
        str.forEachIndexed { idx, chr ->
            if (chr == '{') oCount++
            if (chr == '}') cCount++
            if (oCount == cCount && leftEndIndex == -1) leftEndIndex = idx
        }
        dsLeft = deserialize(str.substring(0, leftEndIndex + 1))
    } else {
        dsLeft = null
        leftEndIndex = 4
    }
    str = str.substring(0, leftEndIndex).removePrefix(",right=")
    var rightEndIndex = -1
    if (str.startsWith('{')) {
        var oCount = 0
        var cCount = 0
        str.forEachIndexed { idx, chr ->
            if (chr == '{') oCount++
            if (chr == '}') cCount++
            if (oCount == cCount && rightEndIndex == -1) rightEndIndex = idx
        }
        dsRight = deserialize(str.substring(0, rightEndIndex + 1))
    } else {
        dsRight = null
        rightEndIndex = 4
    }
    val output = Node(dsValue, dsLeft, dsRight)
    println("D Output: $output")
    return output
}