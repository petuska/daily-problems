package problems.p4.solution

/**
 * Given an array of integers, find the first missing positive integer in linear time and constant space.
 * In other words, find the lowest positive integer that does not exist in the array.
 * The array can contain duplicates and negative numbers as well.
 *
 * For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
 *
 * You can modify the input array in-place.
 */
fun main() {
    assert(solution(listOf(1, 2, 0)) == 3)
    assert(solution(listOf(3, 4, -1, 1)) == 2)
}

private fun solution(input: List<Int>): Int {
    var output = 1
    val checked = mutableSetOf<Int>()
    input.forEach {
        if (it == output) {
            output = it + 1
            checked.add(it)
            var confirmed = false
            do {
                if (checked.contains(output)) {
                    output++
                } else {
                    confirmed = true
                }
            } while (!confirmed)
        }
    }

    println()
    println("Input: $input")
    println("Output: $output")
    return output
}