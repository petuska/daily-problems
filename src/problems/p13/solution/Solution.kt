package problems.p13.solution

/**
 * Given an integer k and a string s, find the length of the longest substring that contains at most k distinct characters.
 *
 * For example, given s = "abcba" and k = 2, the longest substring with k distinct characters is "bcb".
 */

fun main() {
    assert(longestSubstring("abcba", 2) == "bcb")
}

fun longestSubstring(s: String, k: Int): String {
    var longest = ""
    var rolling = ""
    s.forEach {
        rolling += it
        if (countDistinctChars(rolling) <= k) {
            if (rolling.length > longest.length) {
                longest = rolling
            }
        } else {
            rolling = rolling.substring(1)
        }
    }
    return longest
}

fun countDistinctChars(s: String): Int {
    var count = 0
    val checked = mutableSetOf<Char>()
    s.forEach {
        if (!checked.contains(it)) {
            count++
            checked += it
        }
    }
    return count
}