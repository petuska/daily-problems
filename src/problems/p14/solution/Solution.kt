package problems.p14.solution

import kotlin.math.sqrt
import kotlin.random.Random

/**
 * The area of a circle is defined as πr^2. Estimate π to 3 decimal places using a Monte Carlo method.
 *
 * Hint: The basic equation of a circle is x^2 + y^2 = r^2.
 */

fun main() {
    val tests = 100
    var fails = 0
    repeat(tests) {
        if (estimatePI(10000) > 3.100 && estimatePI(1000) < 3.180
                && estimatePI(100000) > 3.120 && estimatePI(1000) < 3.160) {
            fails++
        }
    }
    assert(fails.toDouble() / tests < 0.5)
}

fun estimatePI(n: Int): Double {
    //Define (draw) a square (input domain)
    val squareSize = 1.0
    //"Inscribe" a quadrant. Defined as a function to check if a point is in/out/on the quadrant line using x^2 + y^2 = r^2.
    val quadrant = { point: Pair<Double, Double> -> sqrt(point.first * point.first + point.second * point.second) }

    /*
     * Uniformly scatter a given number of points over the square and
     * count the number of points inside the quadrant, i.e. having a distance from the origin of less than 1
     */
    var insidePointCount = 0.0
    repeat(n) {
        val point = Pair(Random.nextDouble(squareSize), Random.nextDouble(squareSize))
        if (quadrant(point) < squareSize) {
            insidePointCount++
        }
    }

    /*
     * The ratio of the inside-count and the total-sample-count is an estimate of the ratio of the two areas, π/4.
     * Multiply the result by 4 to estimate π.
     */
    val insideOutsideRatio = insidePointCount / n
    return insideOutsideRatio * 4
}