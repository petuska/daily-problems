package problems.p6.solution

import problems.p6.XORList

/**
 * An XOR linked list is a more memory efficient doubly linked list.
 * Instead of each node holding next and prev fields, it holds a field named both, which is an XOR of the next node and the previous node.
 * Implement an XOR linked list; it has an add(element) which adds the element to the end, and a get(index) which returns the node at index.
 *
 * If using a language that has no pointers (such as Python),
 * you can assume you have access to get_pointer and dereference_pointer functions that converts between nodes and memory addresses.
 */
fun main() {
    val xorList = XORListImpl<String>()
    xorList.add("one")
    xorList.add("two")
    xorList.add("three")
    assert(xorList.get(0) == "one")
    assert(xorList.get(1) == "two")
    assert(xorList.get(2) == "three")
}

class XORListImpl<T> : XORList<T>() {
    private val head: XORListNode<T>
    private val tail: XORListNode<T>

    init {
        head = XORListNode(null, null, null)
        tail = XORListNode(head, null, null)
        head.next = tail
    }

    override fun add(element: T) {
        XORListNode(tail.prev, tail, element)
    }

    override fun get(index: Int): T {
        var node = head
        for (i in 0..index) {
            node = node.next!!
        }
        return node.value!!
    }

    internal class XORListNode<T>(var prev: XORListNode<T>?, var next: XORListNode<T>?, val value: T?) {
        init {
            prev?.next = this
            next?.prev = this
        }
    }
}